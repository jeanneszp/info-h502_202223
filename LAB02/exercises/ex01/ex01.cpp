#include<iostream>
#include<string>

//include glad before GLFW to avoid header conflict or define "#define GLFW_INCLUDE_NONE"
#include <glad/glad.h>
#include <GLFW/glfw3.h>


const int width = 500;
const int height = 500;


GLuint compileShader(std::string shaderCode, GLenum shaderType);
GLuint compileProgram(GLuint vertexShader, GLuint fragmentShader);

int main(int argc, char* argv[])
{
	std::cout << "Welcome to exercice one" << std::endl;
	std::cout << "Create a Window and init OpenGL\n"
		"In this exercise you are going to make a window your first rendering pipeline!\n"
		"You need to : \n"
		"\t 1. Initialize the opengl context and create a window using glfw library\n"
		"\t Create 2 functions: \n"
		"\t \t 2. compile_shader : to compile text into a shader \n"
		"\t \t 3. create_program : takes a vertex and a fragment shader and return a pipeline\n"
		"\t 4. Write the code for the fragment and an empty vertex shader\n"
		"\t 5. Create a loop that exit when the window is close and render the pipeline inside\n";

	//Boilerplate
	//1.a Create the OpenGL context 
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	//1.b Create the window
	GLFWwindow* window = glfwCreateWindow(width, height, "Ex01", nullptr, nullptr);

	glfwMakeContextCurrent(window);

	//load openGL function
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		throw std::runtime_error("Message que tu veux (problème GLAD)");
	}

	const float data[9] = {
		//vertices
		-1.0, -1.0, 0.0,
		1.0, -1.0, 0.0,
		0.0, 1.0, 0.0
	};


	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	const std::string sourceV = "#version 330 core\n"
		"in vec3 position;\n"
		"out vec4 color;\n"
		"void main(){ \n"
		"gl_Position = vec4(1.0,1.0,1.0,1.0);\n"
		"color = gl_Position*0.5+0.5;\n"
		"}\n"; 

	const std::string sourceF = "#version 330 core\n"
		"precision mediump float; \n"
		"out vec4 fragColor;\n"
		"in vec4 color;\n"
		"void main() { \n"
		"// 4. Your code: write the minimal code to make the shader compile \n"
		"fragColor = color;\n"
		"} \n";

	GLuint shaderV = compileShader(sourceV, GL_VERTEX_SHADER);
	GLuint shaderF = compileShader(sourceF, GL_FRAGMENT_SHADER);
	GLuint program = compileProgram(shaderV, shaderF);

	
	// 5. Your code: Rendering
	//5.a change the condition of the while by something relevant
	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();
		double now = glfwGetTime();

		glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glfwSwapBuffers(window);

	}

	//clean up ressources

	glfwTerminate();

	return 0;
}

GLuint compileShader(std::string shaderCode, GLenum shaderType)
{
	//2. Your code to compile the shader
	GLuint shader = 0; 
	
	return shader;
}

GLuint compileProgram(GLuint vertexShader, GLuint fragmentShader)
{
	// 3. Your code to compile the program
	GLuint programID = 0;

	
	
	return programID;
}
